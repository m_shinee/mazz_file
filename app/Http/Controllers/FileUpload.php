<?php

namespace App\Http\Controllers;


use App\MP3File;
use Illuminate\Http\Request;

class FileUpload extends Controller
{

    public function imageUpload(Request $request) {
        if ($request->has('file')) {
            $file = $request->file('file');
            $conversationId = $request->get('conversation_id');

            $thumb = $conversationId . '/thumb';
            $original = $conversationId . '/original';

            $filename = $file->getClientOriginalName();

            $response = [];
            $response['thumb'] = $thumb . '/' . $filename;
            $response['duration'] = 0;
            $response['url'] = $response['thumb'];
            $response['file_size'] = $file->getSize();
            $response['filename'] = $filename;

            $file->move($thumb, $file->getClientOriginalName());

            if (!is_dir($original)) {
                mkdir($original, 0777, true);
            }

            copy($response['thumb'], $original . '/'. $file->getClientOriginalName());

            return response()->json($response);
        }
    }

    public function videoUpload(Request $request) {
        if ($request->has('file')) {
            $file = $request->file('file');

            $conversationId = $request->get('conversation_id');

            $path = $conversationId . '/video';

            $filename = $file->getClientOriginalName();
            $fileNameWithOutExtension = pathinfo($filename, PATHINFO_FILENAME);

            if (!strpos($filename, '.mp4')) {
                $filename .= '.mp4';
            }

            $response['filename'] = $filename;
            $response['url'] = $path . '/' . $fileNameWithOutExtension;
            $response['file_size'] = $file->getSize();

            $file->move($path, $filename);

            $getID3 = new \getID3();
            $analyzedFile = $getID3->analyze($path.'/'.$filename);

            $response['duration'] = $analyzedFile['playtime_seconds'];

            copy('img/video.jpg', $response['url'] . '.jpg');

            return response()->json($response);
        }
    }

    public function audioUpload(Request $request) {
        if ($request->has('file')) {
            $file = $request->file('file');

            $conversationId = $request->get('conversation_id');

            $filename = $file->getClientOriginalName();
			
			$filename = str_replace(" ", "", $filename);

            $response['filename'] = $filename;
            $response['url'] = $conversationId . '/' . $filename;
            $response['file_size'] = $file->getSize();

            $file->move($conversationId, $filename);

            $getID3 = new \getID3();
            $analyzedFile = $getID3->analyze($conversationId.'/'.$filename);

            $response['duration'] = $analyzedFile['playtime_seconds'];
            return response()->json($response);
        }
    }

    public function documentUpload(Request $request) {
        if ($request->has('file')) {
            $file = $request->file('file');
            $conversationId = $request->get('conversation_id');

            $filename = $file->getClientOriginalName();
            $response['filename'] = $filename;
            $response['url'] = $conversationId . '/' . $filename;
            $response['duration'] = 0;
            $response['file_size'] = $file->getSize();

            $file->move($conversationId, $filename);
            return response()->json($response);
        }
    }

}