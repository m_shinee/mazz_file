<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
    <form action="imageupload" method="post" enctype="multipart/form-data">
        <input type="file" name="file">
        <input type="hidden" value="1" name="conversation_id">
        <input type="submit" text="Upload File">
    </form>

    <br>
    <br>

    <form action="videoupload" method="post" enctype="multipart/form-data">
        <input type="file" name="file">
        <input type="hidden" value="10" name="conversation_id">
        <input type="submit" text="Upload File">
    </form>

    <br>
    <br>

    <form action="audioupload" method="post" enctype="multipart/form-data">
        <input type="file" name="file">
        <input type="hidden" value="1" name="conversation_id">
        <input type="submit" text="Upload File">
    </form>

    <br>
    <br>

    <form action="documentupload" method="post" enctype="multipart/form-data">
        <input type="file" name="file">
        <input type="hidden" value="1" name="conversation_id">
        <input type="submit" text="Upload File">
    </form>
    </body>
</html>
