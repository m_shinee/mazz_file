<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

Route::get('/', function () {
    return view('welcome');
});

Route::get('twilio', function (Request $request) {
    $identity = $request->input('user_id');
    $conversation_id =  $request->input('conversation_id');

    $twilioAccountSid = 'AC48a022f0e53dbc35367aee18e0d88968';
    $twilioApiKey = 'SKa3d690e5309ced51aeef563a453e1d9d';
    $twilioApiSecret = 'HXOE9elhIuEoirAKcCRzXU4Zc2wCpn0P';
// choose a random username for the connecting user

// Create access token, which we will serialize and send to the client
    $token = new AccessToken(
        $twilioAccountSid,
        $twilioApiKey,
        $twilioApiSecret,
        3600,
        $identity
    );

    $grant = new VideoGrant();
    $grant->setRoom($conversation_id);

    $token->addGrant($grant);

// return serialized token and the user's randomly generated ID
    return response()->json([
        'token' => $token->toJWT()
    ]);
});

Route::get('upload', function () {
    return view('video_upload');
});

Route::post('imageupload', 'FileUpload@imageUpload');

Route::post('videoupload', 'FileUpload@videoUpload');

Route::post('audioupload', 'FileUpload@audioUpload');

Route::post('documentupload', 'FileUpload@documentUpload');